Lista de pendientes

Panel de coordinador
	- Principal
	- CRUD de tutores
	- Reportes
	- CRUD de avisos

Panel de tutores
	- Principal
	- Asignación de tutorados
	- Update de información del tutor
	- CRUD de actividades
	- Reportes
	- Avisos

Panel de tutorados
	- Principal
	- Entrevista
	- Aviso de actividades y confirmación
	- Ficha de "mi tutor"