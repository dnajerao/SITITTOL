			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Area psicopedagògica</h3>
				</div>
				<div class="panel-body">
				
					<div class="form-group">
						<label for="input_psicope1" class="col-sm-4 control-label">¿Cómo te gusta ser? (Autoconcepto)</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope1"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope2" class="col-sm-4 control-label">¿Recibes ayuda en tu casa para la realización de tareas escolares?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope2"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope3" class="col-sm-4 control-label">¿Qué problemas personales intervienen en tus estudios?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope3"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope4" class="col-sm-4 control-label">¿Cuál es tu rendimiento escolar</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope4"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope5" class="col-sm-4 control-label">Menciona las asignaturas que cursas en el semestre actual</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope5"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope6" class="col-sm-4 control-label">¿Cuál es tu asignatura preferida?, ¿Por qué?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope6"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope7" class="col-sm-4 control-label">¿Cuál es la asignatura en la que sobresales?, ¿Por qué?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope7"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope8" class="col-sm-4 control-label">Cuál es tu asignatura con mas bajo promedio del semestre anterior?, ¿Por qué?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope8"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope9" class="col-sm-4 control-label">Por qué vienes al Tecnológico?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope9"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope10" class="col-sm-4 control-label">¿Qué te motiva para venir al Tecnológico?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope10"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope12" class="col-sm-4 control-label">¿Cuál es tu promedio general del ciclo escolar anterior?</label>
						<div class="col-sm-12 col-md-4">
							<input type="number" class="form-control" id="input_psicope12" placeholder="100">
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope13" class="col-sm-4 control-label">¿Tienes asignaturas reprobadas?</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_psicope13">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-5">
							<input type="text" class="form-control" id="input_psicope13_si" placeholder="Especifique cuales">
						</div>
					</div>

					<br>

					<div class="form-group">
						<label for="input_psicope14" class="col-xs-12">Plan de vida y carrera</label>
						<label for="input_psicope14" class="col-sm-4 control-label">¿Cuáles son tus planes inmediatos?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope14"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope15" class="col-sm-4 control-label">¿Cuáles son tus metas en la vida?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope15"></textarea>
						</div>
					</div>

					<br>

					<div class="form-group">
						<label for="input_psicope17" class="col-xs-12">Características personales</label>
						<label for="input_psicope17" class="col-sm-4 control-label">Yo soy ...</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope17"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope18" class="col-sm-4 control-label">Mi carácter es ...</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope18"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope19" class="col-sm-4 control-label">A mi me gusta que ...</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope19"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope20" class="col-sm-4 control-label">Yo aspiro en la vida ...</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope20"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope21" class="col-sm-4 control-label">Yo tengo miedo que ...</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope21"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_psicope22" class="col-sm-4 control-label">Pero penso que podré lograr ...</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_psicope22"></textarea>
						</div>
					</div>
			
					<!-- Guardar -->
					<button type="button" class="btn btn-success pull-right">Guardar</button>
				</div>
			</div>