			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Area social</h3>
				</div>
				<div class="panel-body">
				
					<div class="form-group">
						<label for="input_social1" class="col-sm-4 control-label">¿Còmo es tu relaciòn con los compañeros?</label>
						<div class="col-sm-12 col-md-4">
							<select class="form-control" id="input_social1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Madre">Madre</option>
								<option value="Padre">Padre</option>
								<option value="Hermano">Hermano</option>
								<option value="Otro">Otro</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social2" class="col-sm-4 control-label">¿Por què?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_social2"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social3" class="col-sm-4 control-label">Cómo es tu relación con tus amigos?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_social3"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social4" class="col-sm-4 control-label">¿Tienes pareja?</label>
						<div class="col-sm-12 col-md-4">
							<select class="form-control" id="input_social4">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social5" class="col-sm-4 control-label">¿Cómo es tu relación con tu pareja?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_social5"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social6" class="col-sm-4 control-label">¿Cómo es tu relación con tus profesores?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_social6"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social7" class="col-sm-4 control-label">¿Cómo es tu relación con las autoridades académicas?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_social7"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social8" class="col-sm-4 control-label">¿Qué haces en tu tiempo libre?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_social8"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social9" class="col-sm-4 control-label">¿Cuál es tu actividad recreativa?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_social9"></textarea>
						</div>
					</div>

					<br>

					<!-- indicadores -->
					<div class="form-group">
						<label for="input_social_1" class="col-xs-12">Características personales (madurez y equilibrio). Elije una opción para cada caracteristica.</label>
						<label for="input_social_1" class="col-sm-3 control-label">Puntual</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_1obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_2" class="col-sm-3 control-label">Tímido/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_2">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_2obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_3" class="col-sm-3 control-label">Alegre</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_3">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_3obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_4" class="col-sm-3 control-label">Agresivo/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_4">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_4obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_5" class="col-sm-3 control-label">Abierto/a a las ideas de otros</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_5">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_5obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_6" class="col-sm-3 control-label">Reflexivo/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_6">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_6obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_7" class="col-sm-3 control-label">Constante</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_7">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_7obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_8" class="col-sm-3 control-label">Optimista</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_8">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_8obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_9" class="col-sm-3 control-label">Impulsivo/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_9">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_9obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_10" class="col-sm-3 control-label">Silencioso/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_10">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_10obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_11" class="col-sm-3 control-label">Generoso/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_11">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_11obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_12" class="col-sm-3 control-label">Inquieto/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_12">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_12obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_" class="col-sm-3 control-label">Cambios de humor</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_13obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_14" class="col-sm-3 control-label">Dominante</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_14">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_14obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_15" class="col-sm-3 control-label">Egoísta</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_15">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_15obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_16" class="col-sm-3 control-label">Sumiso/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_16">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_16obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_17" class="col-sm-3 control-label">Confiado/a en si mismo/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_17">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_17obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_18" class="col-sm-3 control-label">Imaginativo/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_18">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_18obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_19" class="col-sm-3 control-label">Con iniciativa propia</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_19">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_19obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_20" class="col-sm-3 control-label">Sociable/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_20">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_20obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_21" class="col-sm-3 control-label">Responsable</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_21">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_21obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_22" class="col-sm-3 control-label">Perseverante/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_22">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_22obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_23" class="col-sm-3 control-label">Motivado/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_23">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_23obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_24" class="col-sm-3 control-label">Activo/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_24">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_24obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_social_25" class="col-sm-3 control-label">Independiente/a</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_social_25">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<textarea class="form-control" rows="1" id="input_social_25obs" placeholder="Observaciones"></textarea>
						</div>
					</div>

					<!-- Guardar -->
					<button type="button" class="btn btn-success pull-right">Guardar</button>
				</div>
			</div>