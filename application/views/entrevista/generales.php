<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Datos generales</h3>
				</div>
				<div class="panel-body">

					<div class="form-group">
						<label for="input_apaterno" class="col-sm-2 control-label">Apellido paterno</label>
						<div class="col-sm-10 col-md-3">
							<input type="text" class="form-control" id="input_apaterno" placeholder="Apellido paterno">
						</div>
						<div class="col-sm-10 col-md-3">
							<input type="text" class="form-control" id="input_amaterno" placeholder="Apellido materno">
						</div>
						<div class="col-sm-12 col-md-4">
							<input type="text" class="form-control" id="input-nombre" placeholder="Nombres(s)">
						</div>
					</div>

					<div class="form-group">
						<label for="input_carrera" class="col-sm-2 control-label">Carrera</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_carrera" placeholder="Carrera">
						</div>
					</div>

					<div class="form-group">
						<label for="input_fnacimiento" class="col-sm-2 control-label">Fecha de nacimiento</label>
						<div class="col-sm-10 col-md-4">
							<input type="text" class="form-control" id="input_fnacimiento" placeholder="DD/MM/AAAA">
						</div>
					</div>

					<div class="form-group">
						<label for="input_sexo" class="col-sm-2 control-label">Sexo</label>
						<div class="col-sm-10 col-md-4">
							<select class="form-control" id="input_sexo">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Hombre">Hombre</option>
								<option value="Mujer">Mujer</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="input_ecivil" class="col-sm-2 control-label">Estado civil</label>
						<div class="col-sm-10 col-md-4">
							<select class="form-control" id="input_ecivil">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Soltero">Soltero/a</option>
								<option value="Casado">Casado/a</option>
								<option value="Otro">Otro</option>
							</select>
						</div>
						<div class="col-sm-10 col-md-6">
							<input type="text" class="form-control" id="input_ecivil_otro" placeholder="Especifique">
						</div>
					</div>

					<div class="form-group">
						<label for="input_estatura" class="col-sm-2 control-label">Estatura</label>
						<div class="col-sm-10 col-md-4">
							<div class="input-group">	
								<input type="text" class="form-control" id="input_estatura" placeholder="1.70">
								<span class="input-group-addon">Mts</span>
							</div>
						</div>
						<label for="input_peso" class="col-sm-2 control-label">Peso</label>
						<div class="col-sm-10 col-md-4">
							<div class="input-group">	
								<input type="text" class="form-control" id="input_peso" placeholder="65">
								<span class="input-group-addon">Kgs</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="input_trabaja" class="col-sm-2 control-label">Trabaja</label>
						<div class="col-sm-10 col-md-4">
							<select class="form-control" id="input_trabaja">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</div>
						<div class="col-sm-10 col-md-6">
							<input type="text" class="form-control" id="input_trabajo_si" placeholder="Especifique">
						</div>
					</div>
		
					<div class="form-group">
						<label for="input_lnacimiento" class="col-sm-2 control-label">Lugar de nacimiento</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_lnacimiento" placeholder="Metepec, Estado de Mèxico">
						</div>
					</div>
		
					<div class="form-group">
						<label for="input_domicilioactual" class="col-sm-2 control-label">Domicilio actual</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_domicilioactual" placeholder="Metepec, Estado de Mèxico">
						</div>
					</div>
		
					<div class="form-group">
						<label for="input_telefono" class="col-sm-2 control-label">Teléfono</label>
						<div class="col-sm-10 col-md-4">
							<input type="tel" class="form-control" id="input_telefono" placeholder="722-123-45-67">
						</div>
						<label for="input_cp" class="col-sm-2 control-label">Código postal</label>
						<div class="col-sm-10 col-md-4">
							<input type="number" class="form-control" id="input_cp" placeholder="50000">
						</div>
					</div>
		
					<div class="form-group">
						<label for="input_tvivienda" class="col-sm-2 control-label">Tipo de vivienda</label>
						<div class="col-sm-10 col-md-4">
							<select class="form-control" id="input_tvivienda">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Departamento">Departamento</option>
								<option value="Casa">Casa</option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label for="input_cvivienda" class="col-sm-12 col-md-4 control-label">La casa o departamento donde vives, es</label>
						<div class="col-sm-12 col-md-4">
							<select class="form-control" id="input_cvivienda">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Propia">Propia</option>
								<option value="Rentada">Rentada</option>
								<option value="Prestada">Prestada</option>
								<option value="Otro">Otro</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-4">
							<input type="text" class="form-control" id="input_trabajo_si" placeholder="Especifique">
						</div>
					</div>
		
					<div class="form-group">
						<label for="input_npersonas" class="col-sm-2 control-label">No. de personas con las que vives</label>
						<div class="col-sm-10 col-md-4">
							<input type="number" class="form-control" id="input_npersonas" placeholder="##">
						</div>
						<label for="input_parentesco" class="col-sm-2 control-label">Parentesco</label>
						<div class="col-sm-10 col-md-4">
							<input type="text" class="form-control" id="input_parentesco" placeholder="Amigos, familiares, ...">
						</div>
					</div>

					<br>

					<!-- Datos del padre -->
					<div class="form-group">
						<label class="col-sm-12">Datos del padre</label>
						<label for="input_nombrep" class="col-sm-2 control-label">Nombre del padre</label>
						<div class="col-sm-10 col-md-6">
							<input type="text" class="form-control" id="input_nombrep" placeholder="Nombre del padre">
						</div>
						<label for="input_edadp" class="col-sm-2 control-label">Edad del padre</label>
						<div class="col-sm-10 col-md-2">
							<input type="number" class="form-control" id="input_edadp" placeholder="##">
						</div>
					</div>
					<div class="form-group">
						<label for="input_trabajap" class="col-sm-2 control-label">Trabaja</label>
						<div class="col-sm-10 col-md-3">
							<select class="form-control" id="input_trabajap">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</div>
						<label for="input_trabajap_si" class="col-sm-3 control-label">Profesión, oficio u ocupación</label>
						<div class="col-sm-10 col-md-4">
							<input type="text" class="form-control" id="input_trabajap_si" placeholder="Especifique">
						</div>
					</div>
					<div class="form-group">
						<label for="input_tipotrabajop" class="col-sm-2 control-label">Tipo de trabajo</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_tipotrabajop" placeholder="Tipo de trabajo">
						</div>
					</div>
					<div class="form-group">
						<label for="input_domiciliop" class="col-sm-2 control-label">Domicilio actual</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_domiciliop" placeholder="Metepec, Estado de Mèxico">
						</div>
					</div>
					<div class="form-group">
						<label for="input_telefonop" class="col-sm-2 control-label">Teléfono</label>
						<div class="col-sm-10 col-md-3">
							<input type="tel" class="form-control" id="input_telefonop" placeholder="722-123-45-67">
						</div>
					</div>

					<br>

					<!-- Datos de la madre -->
					<div class="form-group">
						<label class="col-sm-12">Datos de la madre</label>
						<label for="input_nombrem" class="col-sm-2 control-label">Nombre de la madre</label>
						<div class="col-sm-10 col-md-6">
							<input type="text" class="form-control" id="input_nombrem" placeholder="Nombre de la madre">
						</div>
						<label for="input_edadm" class="col-sm-2 control-label">Edad de la madre</label>
						<div class="col-sm-10 col-md-2">
							<input type="number" class="form-control" id="input_edadm" placeholder="##">
						</div>
					</div>
					<div class="form-group">
						<label for="input_trabajam" class="col-sm-2 control-label">Trabaja</label>
						<div class="col-sm-10 col-md-3">
							<select class="form-control" id="input_trabajam">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</div>
						<label for="input_trabajam_si" class="col-sm-3 control-label">Profesión, oficio u ocupación</label>
						<div class="col-sm-10 col-md-4">
							<input type="text" class="form-control" id="input_trabajam_si" placeholder="Especifique">
						</div>
					</div>
					<div class="form-group">
						<label for="input_tipotrabajom" class="col-sm-2 control-label">Tipo de trabajo</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_tipotrabajom" placeholder="Tipo de trabajo">
						</div>
					</div>
					<div class="form-group">
						<label for="input_domiciliom" class="col-sm-2 control-label">Domicilio actual</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_domiciliom" placeholder="Metepec, Estado de Mèxico">
						</div>
					</div>
					<div class="form-group">
						<label for="input_telefonom" class="col-sm-2 control-label">Teléfono</label>
						<div class="col-sm-10 col-md-3">
							<input type="tel" class="form-control" id="input_telefonom" placeholder="722-123-45-67">
						</div>
					</div>

					<br>

					<!-- Hermanos -->
					<div class="table-responsive">
						<label class="col-sm-12">Nombre de tus hermanos por edad (del mayor al menos, incluyendote)</label>
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<td>#</td>
									<td>Nombre completo</td>
									<td>Fecha de nacimiento</td>
									<td>Sexo</td>
									<td>Estudios</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- ingresos -->
					<div class="form-group">
						<label for="input_ingresosf" class="col-sm-12 col-md-4 control-label">A cuánto ascienden los ingresos de tu familia</label>
						<div class="col-sm-12 col-md-4">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" class="form-control" id="input_ingresosf" placeholder="12345.00">
								<span class="input-group-addon">MXN</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="input_ingresosp" class="col-sm-12 col-md-4 control-label">En caso de ser economicamente independiente, a cuánto ascienden tus ingresos</label>
						<div class="col-sm-12 col-md-4">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" class="form-control" id="input_ingresosp" placeholder="12345.00">
								<span class="input-group-addon">MXN</span>
							</div>
						</div>
					</div>

					<br>

					<div class="form-group">
						<label for="input_primaria" class="col-sm-12">Donde realizaste tus estudios de:</label>
						<label for="input_primaria" class="col-sm-2 control-label">Primaria</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_primaria" placeholder="Metepec, Estado de Mèxico">
						</div>
					</div>
					<div class="form-group">
						<label for="input_secundaria" class="col-sm-2 control-label">Secundaria</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_secundaria" placeholder="Metepec, Estado de Mèxico">
						</div>
					</div>
					<div class="form-group">
						<label for="input_bachillerato" class="col-sm-2 control-label">Bachillerato</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_bachillerato" placeholder="Metepec, Estado de Mèxico">
						</div>
					</div>
					<div class="form-group">
						<label for="input_estsuperiores" class="col-sm-2 control-label">Estudios superiores</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input_estsuperiores" placeholder="Metepec, Estado de Mèxico">
						</div>
					</div>

					<br>

					<div class="form-group">
						<label for="input_pmed" class="col-sm-12 col-md-6 control-label">¿Cuenta con prescripción médica de alguna deficiencia sensorial o funcional que te obligue a llevar aparatos o controlar tu actividad física?</label>
						<div class="col-sm-10 col-md-3">
							<select class="form-control" id="input_pmed">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="input_pmed_si" class="col-sm-2 control-label">Cuales</label>
						<div class="col-sm-12 col-md-4">
							<select class="form-control" id="input_pmed_si">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Vista">Vista</option>
								<option value="Oido">Oido</option>
								<option value="Lenguaje">Lenguaje</option>
								<option value="Otros">Otros</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-6">
							<input type="text" class="form-control" id="input_pmed_otro" placeholder="Especifique">
						</div>
					</div>

					<!-- Guardar -->
					<button type="button" class="btn btn-success pull-right">Guardar</button>
				</div>
			</div>