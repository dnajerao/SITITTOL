			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Areas de integraciòn</h3>
				</div>
				<div class="panel-body">
				
					<div class="form-group">
						<label for="input_integracion1" class="col-xs-12">Area familiar</label>
						<label for="input_integracion1" class="col-sm-4 control-label">¿Cómo es la relación con tu familia?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion1"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="input_integracion2" class="col-sm-4 control-label">¿Existen dificultades?</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_integracion2">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="input_integracion3" class="col-sm-4 control-label">¿De que tipo?</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_integracion3">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Económico">Económico</option>
								<option value="Afectivo">Afectivo</option>
								<option value="Adicciones">Adicciones</option>
								<option value="Otros">Otros</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-5">
							<input type="text" class="form-control" id="input_integracion3_otro" placeholder="Especifique">
						</div>
					</div>

					<div class="form-group">
						<label for="input_integracion4" class="col-sm-4 control-label">¿Qué actitud tienes con tu familia?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion4"></textarea>
						</div>
					</div>

					<br>

					<div class="form-group">
						<label for="input_integracion5" class="col-xs-12">El padre</label>
						<label for="input_integracion5" class="col-sm-4 control-label">¿Cómo te relacionas con tu padre?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion5"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="input_integracion6" class="col-sm-4 control-label">¿Qué actitud tienes hacia tu padre?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion6"></textarea>
						</div>
					</div>

					<br>

					<div class="form-group">
						<label for="input_integracion7" class="col-xs-12">La madre</label>
						<label for="input_integracion7" class="col-sm-4 control-label">¿Cómo te relacionas con tu madre?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion7"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="input_integracion8" class="col-sm-4 control-label">¿Qué actitud tienes hacia tu madre?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion8"></textarea>
						</div>
					</div>

					<br>

					<!-- Hermanos -->
					<div class="form-group">
						<label class="col-sm-12">Hermanos (Con cada uno de ellos</label>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<td>#</td>
									<td>Nombre del hermano</td>
									<td>Relación</td>
									<td>Actitud</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="form-group">
						<label for="input_integracion9" class="col-sm-4 control-label">¿Con quién te sientes más ligado afectivamente?</label>
						<div class="col-sm-12 col-md-3">
							<select class="form-control" id="input_integracion9">
								<option selected="true" disabled>Elegir opción</option>
								<option value="Madre">Madre</option>
								<option value="Padre">Padre</option>
								<option value="Hermano">Hermano</option>
								<option value="Otro">Otro</option>
							</select>
						</div>
						<div class="col-sm-12 col-md-5">
							<input type="text" class="form-control" id="input_integracion9_otro" placeholder="Especifique">
						</div>
					</div>

					<div class="form-group">
						<label for="input_integracion10" class="col-sm-4 control-label">Especifica por que</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion10"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_integracion11" class="col-sm-4 control-label">¿Quién se ocupa mas directamente de tu educación?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion11"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_integracion12" class="col-sm-4 control-label">¿Quién ha influido más en tu decisión para estudiar esta carrera?</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion12"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="input_integracion13" class="col-sm-4 control-label">Consideras importante facilitar algun otro dato sobre tu ambiente familiar</label>
						<div class="col-sm-12 col-md-8">
							<textarea class="form-control" rows="2" id="input_integracion13"></textarea>
						</div>
					</div>
				
					<!-- Guardar -->
					<button type="button" class="btn btn-success pull-right">Guardar</button>
				</div>
			</div>