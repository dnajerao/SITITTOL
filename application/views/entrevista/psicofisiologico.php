			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Estado psicofisiológico</h3>
				</div>
				<div class="panel-body">
				
					<div class="form-group">
					<label for="input_indicador1" class="col-xs-12">Elija una opciòn para los siguientes indicadores</label>
						<label for="input_indicador1" class="col-sm-3 control-label">Manos y/o pies hinchados</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Dolores en el vientre</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Dolores de cabeza y/o vòmitos</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Pèrdida del equilibrio</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Fatiga y agotamiento</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Pèrdida de vista u oido</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Dificultades para dormir</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Pesadillas o terrores nocturnos</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Incontinencia (orina/heces)</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Tartamudeos al explicarse</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicador1" class="col-sm-3 control-label">Miedos intensos ante cosas</label>
						<div class="col-sm-9 col-md-4">
							<select class="form-control" id="input_indicador1">
								<option selected="true" disabled>Elegir opción</option>
								<option value="No">No</option>
								<option value="Poco">Poco</option>
								<option value="Frecuente">Frecuente</option>
								<option value="Mucho">Mucho</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="input_indicobs" class="col-sm-3 control-label">Observaciones de higiene</label>
						<div class="col-sm-12 col-md-9">
							<textarea class="form-control" rows="3" id="input_indicobs"></textarea>
						</div>
					</div>

					<!-- Guardar -->
					<button type="button" class="btn btn-success pull-right">Guardar</button>
				</div>
			</div>