<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Sistema de Tutorias</title>

	<!-- Bootstrap -->
	<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/generales.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/navbar-custom.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/login.css" rel="stylesheet">
</head>
<body>

	<?php include('header.php'); ?>
	
	<!-- navbar -->
	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Portal de Tutorias</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="#">Iniciar Sesión</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</div>

	<!-- Formulario inicio de sesión -->
	<div class="container">
		<div class="col-md-offset-3 col-sm-12 col-md-6 loginForm">
			<form role="form" id="loginForm" name="loginForm" method="POST" action="#">

				<!-- Usuario -->
				<div class="form-group">
					<label for="input_usuario">Usuario</label>
					<input id="input_usuario"
						name="input_usuario"
						type="text"
						class="form-control"
						placeholder="alu_12345678"
						required
						pattern="^alu_[\w]*$||^ALU_[\w]*$||^Alu_[\w]*$||^ALu_[\w]*$||^aLu_[\w]*$||^alU_[\w]*$||^aLU_[\w]*$||^V[\w]*$||^v[\w]*$"
						autofocus
						maxlength="13"
						autocomplete="off">
				</div>

				<!-- Password -->
				<div class="form-group">
					<label for="input_pswd">Contraseña</label>
					<input id="input_pswd"
						name="input_pswd"
						type="password"
						class="form-control"
						placeholder="pw0000"
						required
						pattern="^pw[0-9]{1,4}$||^PW[0-9]{1,4}$||^Pw[0-9]{1,4}$||^pW[0-9]{1,4}$"
						maxlength="6">
				</div>

				<div class="form-group">
					<label for="input_rol">Ingresar como</label>
					<select class="form-control" id="input_rol">
						<option selected="true" disabled>Elegir opción</option>
						<option value="0">Alumno</option>
						<option value="1">Tutor</option>
						<option value="2">Coordinador</option>
					</select>
				</div>

				<!-- Captcha -->
				<!--
				<div id="divCaptcha" class="form-group">
					<label for="inpCaptcha">Código</label>
					<label>
						<img id="captcha" src="../../app/portalLogin/captcha.png" alt="Captcha" height="45" width="255">
						<button id="btnCaptcha" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
						</button>
					</label>
					<input id="inpCaptcha"
						name="inpCaptcha"
						type="text"
						class="form-control"
						placeholder="Ingrese el Código"
						required
						autocomplete="off">
				</div>
				-->

				<button class="btn btn-lg btn-primary btn-block" type="submit" id="btnIngresar" name="btnIngresar">
					Ingresar
					<i id="spin_carga" class="fa fa-spinner fa-spin"></i>
				</button>
			</form>

		</div>
	</div>

	<?php include('footer.php'); ?>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
</body>
</html>