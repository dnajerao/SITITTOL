
	<div class="container">
		<footer class="portal-footer">
			<label >
				Instituto Tecnológico de Toluca | <a href="www.toluca.tecnm.mx">www.toluca.tecnm.mx</a>
				<br>
				Instituto Tecnológico de Toluca - Algunos derechos reservados © 2017
				<br>
				<br>
			</label>

			<img class="img-responsive center-block" alt="SEP" src="<?php echo base_url(); ?>assets/img/footer-sep.png">
			<label>
				Av. Tecnológico s/n. Fraccionamiento La Virgen
				<br>
				Metepec, Edo. De México, México C.P. 52149
				<br>
				Tel. (52) (722) 2 08 72 00
			</label>
		</footer>
	</div>