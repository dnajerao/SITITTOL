<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Sistema de Tutorias</title>

	<!-- Bootstrap -->
	<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/generales.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/navbar-custom.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/portal.css" rel="stylesheet">
</head>
<body>
	
	<?php include('header.php'); ?>

	<!-- navbar -->
	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand">Sistema de Tutorias</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="<?php echo site_url('portal/entrevista'); ?>">Entrevista</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</div>
	</nav>

	<!-- Inicio de formulario -->
	<div class="container">	
		<form class="form-horizontal">

			<ul id="tabsEntrevista" class="nav nav-pills">
				<li role="presentation" class="active"><a href="#general" data-toggle="tab">General</a></li>
				<li role="presentation"><a href="#psicofisiologico" data-toggle="tab">Estado psicofisiológico</a></li>
				<li role="presentation"><a href="#integracion" data-toggle="tab">Areas de integración</a></li>
				<li role="presentation"><a href="#social" data-toggle="tab">Area social</a></li>
				<li role="presentation"><a href="#psicopedagogica" data-toggle="tab">Area psicopedagógica</a></li>
			</ul>
		
			<div class="tab-content clearfix">
				<div class="tab-pane active" id="general">
					<?php $this->load->view('entrevista/generales'); ?>
				</div>
		
				<div class="tab-pane" id="psicofisiologico">
					<?php $this->load->view('entrevista/psicofisiologico'); ?>
				</div>
		
				<div class="tab-pane" id="integracion">
					<?php $this->load->view('entrevista/integracion'); ?>
				</div>
		
				<div class="tab-pane" id="social">
					<?php $this->load->view('entrevista/social'); ?>
				</div>
		
				<div class="tab-pane" id="psicopedagogica">
					<?php $this->load->view('entrevista/psicopedagogica'); ?>
				</div>
				
			</div>
		
		</form>

	</div>


	<!-- Fin de formulario -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>js/funciones.js"></script>
</body>
</html>